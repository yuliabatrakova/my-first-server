var express = require('express');
var router = express.Router();
const db = require('../repositories/userlist.json')

const { saveName, updateName } = require("../services/user.service");
const { deleteData }= require("../repositories/user.repository")
const { isAuthorized } = require("../middlewares/auth.middleware");

router.get(`/user`, (req, res, next) => {
	res.json(db)
})

router.get(`/user/:id`, (req, res, next) => {
	res.json(db.some(el => el._id === req.params.id) ? db.find(el => el._id === req.params.id)
	 : "No such user is found")
})

router.post(`/user`, (req, res, next) => {
  const result = saveName(req.body);
  if (result) {
    res.json(`Your name is ${result} and your data is saved`);
  } else {
    res.status(400).send(`Some error`);
  }
});

router.put(`/user/:id`, (req, res) => {
	if(req.params.id === req.body._id) {
		res.json(db.some(el => el._id === req.params.id) ? updateName(req.body) :
	 "No such element is found")
	} else {
		res.json("Make sure the id in request.params matches _id in request.body")
	}
})
router.delete(`/user/:id`, (req, res) => {
		res.json(db.some(el => el._id === req.params.id) ? deleteData(req.params.id) :
	 "No such element is found")
		console.log(db)
})
module.exports = router;
