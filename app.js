var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());


app.use('/', indexRouter);
app.get('/user', usersRouter);
app.get('/user/:id', usersRouter);
app.post('/user', usersRouter);
app.put('/user/:id', usersRouter);
app.delete('/user/:id', usersRouter);
module.exports = app;
