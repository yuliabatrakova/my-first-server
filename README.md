# my-first-server
Also deployed on Heroku: https://yuliabatrakova-my-first-server.herokuapp.com/
----------------
This server was created with ❤️
----------------
Feel free to use Postman to test it.
----------------
Endpoints:

For **GET**: '/user' to get names of all users,
'user/:id' to get information about a user on id(where ':id' is a number)

For **POST**: '/user' to post a new user. Keep in mind that your request body must include keys 'id'(with an underscore before it) and 'name' to post a user successfully.

For **PUT**: '/user/:id to update an existing user. Make sure the id in request.params matches id(with an underscore before it) in request.body.

For **DELETE**: '/user/:id to delete a user