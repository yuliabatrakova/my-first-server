const db = require('../repositories/userlist.json')
const saveData = (data) => {
  // код по сохранению данных в БД
  if (data._id && data.name) {
    	db.push(data)
    	console.log(db)
  	    return `${data.name}`
  	} else {
    return false;
  }
}
const updateData = (data) => {
	if (data._id && data.name) {
		db[data._id - 1] = data
		console.log(db)
	return `${data.name} is updated`
} else {
    return false;
  }
}
const deleteData = (id) => {
	db.splice(id - 1, 1)
	return `id ${id} is deleted`
}
module.exports = {
  saveData,
  updateData,
  deleteData
};