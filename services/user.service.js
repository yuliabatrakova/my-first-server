const { saveData, updateData } = require("../repositories/user.repository");

const getName = (user) => {
  if (user) {
    return user.name;
  } else {
    return null;
  }
};

const saveName = (user) => {
  if (user) {
    return saveData(user);
  } else {
    return null;
  }
};

const updateName = (user) => {
  if (user) {
    return updateData(user);
  } else {
    return null;
  }
};
const deleteName = (id) => {
  if (id) {
    return deleteData(id);
  } else {
    return null;
  }
};
module.exports = {
  getName,
  saveName,
  updateName,
  deleteName
};